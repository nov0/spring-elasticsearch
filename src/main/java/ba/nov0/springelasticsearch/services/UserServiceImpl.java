package ba.nov0.springelasticsearch.services;

import ba.nov0.springelasticsearch.model.User;
import ba.nov0.springelasticsearch.repositories.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public void delete(Integer id) {
        userRepository.deleteById(id);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public Page<User> getUsers(String searchQuery, Pageable pageable) {
        return userRepository.searchUsers(searchQuery, pageable);
    }

    @Override
    public Optional<User> getById(Integer id) {
        return userRepository.findById(id);
    }
}
