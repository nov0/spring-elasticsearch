package ba.nov0.springelasticsearch.services;

import ba.nov0.springelasticsearch.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User save(User user);

    void delete(Integer id);

    void delete(User user);

    List<User> getAll();

    Optional<User> getById(Integer id);

    Page<User> getUsers(String searchQuery, Pageable pageable);
}
