package ba.nov0.springelasticsearch.utils;

import ba.nov0.springelasticsearch.elasticsearch.documents.UserDocument;
import ba.nov0.springelasticsearch.elasticsearch.documents.UserSearchResult;
import ba.nov0.springelasticsearch.model.User;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UserDocumentConvertUtility extends ObjectConvertUtility {

    public static UserDocument convertHitToUserDocument(SearchHit hit) {
        UserDocument userDocument = new UserDocument();
        Map<String, Object> source = hit.getSourceAsMap();
        userDocument.setId(hit.getId());
        userDocument.setActive(getBooleanFormObject(source.get(UserFields.ACTIVE.getName())));
        userDocument.setUserId(getIntegerFormObject(source.get(UserFields.USER_ID.getName()).toString()));
        userDocument.setBirthDate(getDateFromObject(source.get(UserFields.BIRTH_DATE.getName())));
        userDocument.setFirstName(getStringFromObject(source.get(UserFields.FIRST_NAME.getName())));
        userDocument.setLastName(getStringFromObject(source.get(UserFields.LAST_NAME.getName())));
        userDocument.setImageUrl(getStringFromObject(source.get(UserFields.IMAGE_URL.getName())));
        return userDocument;
    }

    public static UserSearchResult convertSearchResponse(SearchResponse response) {
        UserSearchResult searchResult = new UserSearchResult();
        searchResult.setUsers(convertListOfHitsToListOfUserDocuments(response.getHits().getHits()));
        searchResult.setTotal(getTotalHits(response));
        searchResult.setNumberOfResults(response.getHits().getHits().length);
        searchResult.setTook(response.getTook().getMillis());
        return searchResult;
    }

    public static List<UserDocument> convertListOfHitsToListOfUserDocuments(SearchHit[] hits) {
        return Arrays.stream(hits)
                .map(UserDocumentConvertUtility::convertHitToUserDocument)
                .collect(Collectors.toList());
    }

    public static Map<String, Object> convertUserDocumentToMap(UserDocument userDocument) {
        Map<String, Object> mappedUser = new HashMap<>();
        if (!StringUtils.isEmpty(userDocument.getId()))
            mappedUser.put(UserFields.ID.getName(), userDocument.getId());
        if (userDocument.getUserId() != null)
            mappedUser.put(UserFields.USER_ID.getName(), userDocument.getUserId());
        convertUserDocumentToMapForUpdate(userDocument, mappedUser);
        return mappedUser;
    }

    public static Map<String, Object> convertUserDocumentToMapForUpdate(UserDocument userDocument, Map<String, Object> mappedUser) {
        if (userDocument.getBirthDate() != null)
            mappedUser.put(UserFields.BIRTH_DATE.getName(), convertDateToElastic(userDocument.getBirthDate()));
        if (!StringUtils.isEmpty(userDocument.getFirstName()))
            mappedUser.put(UserFields.FIRST_NAME.getName(), userDocument.getFirstName());
        if (!StringUtils.isEmpty(userDocument.getLastName()))
            mappedUser.put(UserFields.LAST_NAME.getName(), userDocument.getLastName());
        if (!StringUtils.isEmpty(userDocument.getImageUrl()))
            mappedUser.put(UserFields.IMAGE_URL.getName(), userDocument.getImageUrl());
        mappedUser.put(UserFields.ACTIVE.getName(), userDocument.isActive());
        return mappedUser;
    }

    public static UserDocument convertUserToUserDocument(User user) {
        if (user == null) return null;
        UserDocument userDocument = new UserDocument();
        userDocument.setUserId(user.getId());
        userDocument.setBirthDate(user.getBirthDate());
        userDocument.setActive(user.isActive());
        userDocument.setFirstName(user.getFirstName());
        userDocument.setLastName(user.getLastName());
        userDocument.setImageUrl(user.getImageUrl());
        return userDocument;
    }

    public static UserDocument getUserDocumentFormGetResponse(GetResponse getResponse) {
        UserDocument userDocument = null;
        if (getResponse.isExists() && !getResponse.isSourceEmpty()) {
            Map<String, Object> sourceAsMap = getResponse.getSourceAsMap();
            userDocument = new UserDocument();
            userDocument.setId(getResponse.getId());
            userDocument.setUserId(getIntegerFormObject(sourceAsMap.get(UserFields.USER_ID.getName())));
            userDocument.setFirstName(getStringFromObject(sourceAsMap.get(UserFields.FIRST_NAME.getName())));
            userDocument.setLastName(getStringFromObject(sourceAsMap.get(UserFields.LAST_NAME.getName())));
            userDocument.setImageUrl(getStringFromObject(sourceAsMap.get(UserFields.IMAGE_URL.getName())));
            userDocument.setBirthDate(getDateFromObject(sourceAsMap.get(UserFields.BIRTH_DATE.getName())));
            userDocument.setActive(getBooleanFormObject(sourceAsMap.get(UserFields.ACTIVE.getName())));
        }
        return userDocument;
    }

    private static long getTotalHits(SearchResponse response) {
        return response.getHits() != null && response.getHits().getHits().length > 0 ?
                response.getHits().getTotalHits() : 0;
    }

}
