package ba.nov0.springelasticsearch.utils;

public enum UserFields {

    ID("id"),
    USER_ID("userId"),
    FIRST_NAME("firstName"),
    LAST_NAME("lastName"),
    BIRTH_DATE("birthDate"),
    ACTIVE("active"),
    IMAGE_URL("imageUrl");

    UserFields(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }


}
