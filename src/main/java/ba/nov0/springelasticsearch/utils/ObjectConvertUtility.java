package ba.nov0.springelasticsearch.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ObjectConvertUtility {
    private final static Logger logger = LoggerFactory.getLogger(UserDocumentConvertUtility.class);

    private static SimpleDateFormat formatElastic = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    public static Date getDateFromObject(Object object) {
        Date date = null;
        if (object == null) return date;
        try {
            date = formatElastic.parse(object.toString());
        } catch (ParseException e) {
            logger.error("Error while parsing date: " + object);
        }
        return date;
    }

    public static String convertDateToElastic(Date dateOld) {
        String date = null;
        if (dateOld == null) return date;
        try {
            date = formatElastic.format(dateOld);
        } catch (Exception e) {
            logger.error("Error while parsing date: " + dateOld);
        }
        return date;
    }

    public static String getStringFromObject(Object object) {
        String value = null;
        if (object != null) {
            value = object.toString();
        }
        return value;
    }

    public static Integer getIntegerFormObject(Object object) {
        Integer value = null;
        if (object != null) {
            value = Integer.parseInt(object.toString());
        }
        return value;
    }

    public static boolean getBooleanFormObject(Object object) {
        boolean value = false;
        if (object != null) {
            value = (boolean) object;
        }
        return value;
    }
}
