package ba.nov0.springelasticsearch.dao;

import ba.nov0.springelasticsearch.elasticsearch.documents.UserDocument;
import ba.nov0.springelasticsearch.elasticsearch.services.UserDocumentService;
import ba.nov0.springelasticsearch.model.User;
import ba.nov0.springelasticsearch.services.UserService;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class UserDaoService {

    private final UserService userService;
    private final UserDocumentService userDocumentService;

    public UserDaoService(UserService userService, UserDocumentService userDocumentService) {
        this.userService = userService;
        this.userDocumentService = userDocumentService;
    }

    public List<User> getAll() {
        return userService.getAll();
    }

    public UserDocument save(User user) throws IOException {
        User savedUser = userService.save(user);
        return userDocumentService.save(savedUser);
    }

    public Optional<User> getById(Integer id) {
        return userService.getById(id);
    }

    public UserDocument update(User user, String elasticId) throws IOException {
        User updatedUser = userService.save(user);
        return userDocumentService.update(updatedUser, elasticId);
    }

    public void delete(String documentId) {
        UserDocument userDocument = userDocumentService.getDocumentById(documentId);
        Integer userId = userDocument.getUserId();
        userDocumentService.delete(userDocument.getId());
        userService.delete(userId);
    }

    public void copyAll() throws IOException {
        List<User> users = userService.getAll();
        for (User user : users) {
            userDocumentService.save(user);
        }
    }
}
