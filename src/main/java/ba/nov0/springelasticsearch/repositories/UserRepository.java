package ba.nov0.springelasticsearch.repositories;

import ba.nov0.springelasticsearch.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;


public interface UserRepository extends JpaRepository<User, Integer> {

    @Query("SELECT u FROM User u WHERE" +
            " u.firstName LIKE CONCAT('%', :searchQuery, '%') " +
            " OR u.lastName LIKE CONCAT('%', :searchQuery, '%') " +
            " OR u.address LIKE CONCAT('%', :searchQuery, '%') " +
            " OR u.email LIKE CONCAT('%', :searchQuery, '%') GROUP BY u.id"
    )
    Page<User> searchUsers(@Param("searchQuery") String searchQuery, Pageable pageable);
}
