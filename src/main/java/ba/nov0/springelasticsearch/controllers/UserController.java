package ba.nov0.springelasticsearch.controllers;

import ba.nov0.springelasticsearch.dao.UserDaoService;
import ba.nov0.springelasticsearch.elasticsearch.documents.UserDocument;
import ba.nov0.springelasticsearch.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("user")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final String PATH_ID = "/{id}";

    private final UserDaoService userDaoService;

    public UserController(UserDaoService userDaoService) {
        this.userDaoService = userDaoService;
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    @GetMapping
    public ResponseEntity getAll() {
        return ResponseEntity.ok(userDaoService.getAll());
    }

    // edit method
    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    @PutMapping
    public ResponseEntity update(@RequestParam(name = "id", required = false) String elasticId, @RequestBody User user) {
        if (user.getId() != null) {
            try {
                return ResponseEntity.ok(userDaoService.update(user, elasticId));
            } catch (IOException e) {
                logger.error("Error while updating user");
                return ResponseEntity.badRequest().build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
        }
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    @PostMapping
    public ResponseEntity save(@RequestBody User user) {
        try {
            UserDocument userDocument = userDaoService.save(user);
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path(PATH_ID)
                    .buildAndExpand(userDocument.getUserId()).toUri();
            return ResponseEntity.created(location).body(userDocument);
        } catch (IOException e) {
            logger.error("Error while saving user" + e.getLocalizedMessage());
            return ResponseEntity.badRequest().build();
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @GetMapping(PATH_ID)
    public ResponseEntity getById(@PathVariable(name = "id") Integer id) {
        return Optional.ofNullable(userDaoService.getById(id))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    @DeleteMapping("/{documentId}")
    public ResponseEntity deleteById(@PathVariable(name = "documentId") String documentId) {
        if (!StringUtils.isEmpty(documentId)) {
            userDaoService.delete(documentId);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/copy-all-users")
    public ResponseEntity copyAll() throws Exception {
        userDaoService.copyAll();
        Map<String, String> response = new HashMap<>();
        response.put("status", "OK");
        response.put("message", "All users are copied to elastic search.");
        return ResponseEntity.ok(response);
    }

}
