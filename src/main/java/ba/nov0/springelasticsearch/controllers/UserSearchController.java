package ba.nov0.springelasticsearch.controllers;

import ba.nov0.springelasticsearch.elasticsearch.services.UserDocumentSearchService;
import ba.nov0.springelasticsearch.model.Pagination;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("user/search")
public class UserSearchController {

    private final UserDocumentSearchService userDocumentSearchService;

    public UserSearchController(UserDocumentSearchService userDocumentSearchService) {
        this.userDocumentSearchService = userDocumentSearchService;
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    @PostMapping
    public ResponseEntity search(@RequestBody Pagination pagination) throws IOException {
        return ResponseEntity.ok(userDocumentSearchService.getPaginated(pagination));
    }

}
