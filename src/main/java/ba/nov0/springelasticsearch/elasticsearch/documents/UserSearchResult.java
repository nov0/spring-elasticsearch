package ba.nov0.springelasticsearch.elasticsearch.documents;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserSearchResult {

    private Long numberOfResults;
    private long total;
    private long took;
    private List<UserDocument> users = new ArrayList<>();

    public long getNumberOfResults() {
        return numberOfResults;
    }

    public void setNumberOfResults(long numberOfResults) {
        this.numberOfResults = numberOfResults;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getTook() {
        return took;
    }

    public void setTook(long took) {
        this.took = took;
    }

    public List<UserDocument> getUsers() {
        return users;
    }

    public void setUsers(List<UserDocument> users) {
        this.users = users;
    }
}
