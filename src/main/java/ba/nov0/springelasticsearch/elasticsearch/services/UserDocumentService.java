package ba.nov0.springelasticsearch.elasticsearch.services;

import ba.nov0.springelasticsearch.elasticsearch.documents.UserDocument;
import ba.nov0.springelasticsearch.model.User;
import ba.nov0.springelasticsearch.utils.UserDocumentConvertUtility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.HashMap;

@Service
public class UserDocumentService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public static final String INDEX_NAME = "users";
    private static final String INDEX_TYPE = "_doc";

    private final RestHighLevelClient restHighLevelClient;
    private final ObjectMapper objectMapper;

    public UserDocumentService(RestHighLevelClient restHighLevelClient, ObjectMapper objectMapper) {
        this.restHighLevelClient = restHighLevelClient;
        this.objectMapper = objectMapper;
    }

    public UserDocument save(User user) throws IOException {
        UserDocument userDocument = UserDocumentConvertUtility.convertUserToUserDocument(user);
        if (userDocument == null) return null;

        IndexRequest request = new IndexRequest(INDEX_NAME);
        request.source(UserDocumentConvertUtility.convertUserDocumentToMap(userDocument));
        // need for v6.5.4
        request.type(INDEX_TYPE);
        IndexResponse indexResponse = restHighLevelClient.index(request, RequestOptions.DEFAULT);

        userDocument.setId(indexResponse.getId());
        logger.info("Document created ID: " + userDocument.getId());
        return userDocument;
    }

    public UserDocument update(User user, String elasticId) throws IOException {
        UserDocument userDocument = UserDocumentConvertUtility.convertUserToUserDocument(user);
        if (userDocument == null || StringUtils.isEmpty(elasticId)) return null;

        UpdateRequest updateRequest = new UpdateRequest(INDEX_NAME, INDEX_TYPE, elasticId);
        updateRequest.doc(UserDocumentConvertUtility.convertUserDocumentToMapForUpdate(userDocument, new HashMap<>()));

        UpdateResponse updateResponse = restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
        logger.info("Update status: " + updateResponse.getResult().getLowercase() + " for document ID: " + elasticId);
        return userDocument;
    }

    public void delete(String indexId) {
        try {
            DeleteRequest deleteRequest = new DeleteRequest(INDEX_NAME, INDEX_TYPE, indexId);
            restHighLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            logger.error("Error while deleting document form index: " + INDEX_NAME + ", document ID: " + indexId);
        }
    }

    public UserDocument getDocumentById(String documentId) {
        GetRequest getRequest = new GetRequest(INDEX_NAME, INDEX_TYPE, documentId);
        try {
            GetResponse getResponse = restHighLevelClient.get(getRequest, RequestOptions.DEFAULT);
            return UserDocumentConvertUtility.getUserDocumentFormGetResponse(getResponse);
        } catch (IOException e) {
            logger.error("Error while getting document from index: " + INDEX_NAME + ", document ID: " + documentId);
        }
        return null;
    }
}
