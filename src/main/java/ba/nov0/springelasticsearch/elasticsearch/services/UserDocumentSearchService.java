package ba.nov0.springelasticsearch.elasticsearch.services;

import ba.nov0.springelasticsearch.elasticsearch.documents.UserSearchResult;
import ba.nov0.springelasticsearch.model.Pagination;
import ba.nov0.springelasticsearch.utils.UserFields;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static ba.nov0.springelasticsearch.utils.UserDocumentConvertUtility.convertSearchResponse;

@Service
public class UserDocumentSearchService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private static final String LOGGER_MESSAGE = "Search successfully executed. query: %s, Status: %s, index name: %s, took: %s, results: %d, skipped shards: %s, from result: %d";
    private static final int RESULT_PER_PAGE = 50;

    private final RestHighLevelClient restHighLevelClient;

    public UserDocumentSearchService(RestHighLevelClient restHighLevelClient) {
        this.restHighLevelClient = restHighLevelClient;
    }

    public UserSearchResult getPaginated(Pagination pagination) throws IOException {

        int fromResult = getPagination(pagination.getPageIndex(), pagination.getPageSize());

        SearchRequest searchRequest = getSearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        QueryBuilder query = createQuery(pagination.getQuery());

        searchSourceBuilder.query(query);
        searchSourceBuilder.size(pagination.getPageSize());
        searchSourceBuilder.sort(new FieldSortBuilder(pagination.getColumnName()).order(getSortOrder(pagination.getOrder())));
        searchSourceBuilder.from(fromResult);
        searchRequest.source(searchSourceBuilder);

        SearchResponse response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        logger.info(getLoggerMessage(response, "string_query", fromResult, pagination.getPageSize()));

        return convertSearchResponse(response);
    }

    private int getPagination(Integer page, Integer resultPerPage) {
        if (page == null) page = 0;
        return (resultPerPage != null ? resultPerPage : RESULT_PER_PAGE) * page;
    }

    private SearchRequest getSearchRequest() {
        return new SearchRequest(UserDocumentService.INDEX_NAME);
    }

    private String getLoggerMessage(SearchResponse response, String queryType, int fromResults, int resultPerPage) {
        return String.format(LOGGER_MESSAGE,
                queryType,
                response.status(),
                UserDocumentService.INDEX_NAME,
                response.getTook(),
                resultPerPage,
                response.getSkippedShards(),
                fromResults);
    }

    private SortOrder getSortOrder(String sortOrder) {
        return StringUtils.isEmpty(sortOrder) || sortOrder.toLowerCase().equals("asc") ? SortOrder.ASC : SortOrder.DESC;
    }

    private QueryBuilder createQuery(String queryWord) {
        if (StringUtils.isEmpty(queryWord)) {
            return QueryBuilders.matchAllQuery();
        } else {
            QueryStringQueryBuilder queryStringQueryBuilder = QueryBuilders.queryStringQuery("*" + StringUtils.uncapitalize(queryWord) + "*" + " *" + StringUtils.capitalize(queryWord) + "*");
            Map<String, Float> fields = new HashMap<>();
            fields.put(UserFields.FIRST_NAME.getName(), 1.0f);
            fields.put(UserFields.LAST_NAME.getName(), 1.0f);
            queryStringQueryBuilder.fields(fields);
            return queryStringQueryBuilder;
        }
    }


    // low-lever api request
    /*public Response getSomething() throws IOException {
        Request request = new Request(
                "GET",
                "/shakespeare/_search");
        request.addParameter("size", "100");
        request.addParameter("pretty", "true");
        request.addParameter("q", "*");
        return restClient.performRequest(request);
    }*/

}
