
##Prepare elastic search
Created template for dates
```
PUT _template/tpl
{
  "index_patterns": "user*",
  "mappings": {
    "dynamic_date_formats": [
      "yyyy-MM-dd'T'HH:mm:ssX||yyyy-MM-dd'T'HH:mm:ss.SX||yyyy-MM-dd'T'HH:mm:ss.SSX||yyyy-MM-dd'T'HH:mm:ss.SSSX"
    ]
  }
}
```

Add mappings for user:
```
PUT users
{
  "mappings": {
    "properties": {
      "birthDate": {
        "type": "date",
        "format": "yyyy-MM-dd'T'HH:mm:ss.SX"
      },
      "firstName": {
        "type":"keyword"
      },
      "lastName": {
        "type":"keyword"
      }
    }
  }
}
```

mappings for Bonsai search:
```
PUT users 
{
  "mappings": {
    "_doc": { 
      "properties": { 
        "firstName":    { "type": "keyword"  }, 
        "lastName":     { "type": "keyword"  }, 
        "imageUrl":     { "type": "text"},
        "active":       { "type": "boolean"},
        "userId":       { "type": "integer"},
        "birthDate":  {
          "type":   "date", 
          "format": "yyyy-MM-dd'T'HH:mm:ss"
        }
      }
    }
  }
}
```